// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "StructurePatternsGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTUREPATTERNS_API AStructurePatternsGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
