// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProxyUser.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTUREPATTERNS_API UProxyUser : public UObject
{
	GENERATED_BODY()
	
public:
	bool PublicUserBool;
	float PublicUserFloat;
};
