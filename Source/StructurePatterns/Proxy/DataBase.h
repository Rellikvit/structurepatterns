// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "DataBase.generated.h"

UCLASS()
class UDataBase: public UObject
{
	GENERATED_BODY()
public:
	UDataBase();

	static UDataBase* GetInstance();

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ConfigInstance")
	bool ValuableGlobalBoolVar;

	UPROPERTY(EditAnywhere,BlueprintReadWrite,Category = "ConfigInstance")
	float ValuableGlobalFloatVar;

private:
	static UDataBase* Instance;
};

inline UDataBase::UDataBase()
{
	//Init data
}

inline UDataBase* UDataBase::GetInstance()
{
	if(!Instance)
	{
		Instance = NewObject<UDataBase>();
	}
	return Instance;
}
