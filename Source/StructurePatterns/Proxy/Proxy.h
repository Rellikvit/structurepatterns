// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "ProxyUser.h"
#include "Proxy.generated.h"

UCLASS()
class  UProxy : public UObject
{
	GENERATED_BODY()
public:
	UProxy();

	void RefreshVariables();
	void RefreshUser();

	UProxyUser* User;

	float ValuableForObjectFloat;
	bool ValuableForObjectBool;
};

inline UProxy::UProxy()
{
	//Init Proxy
}

inline void UProxy::RefreshVariables()
{
	//Refresh Proxy Variables;

	RefreshUser();
}

inline void UProxy::RefreshUser()
{
	User->PublicUserBool = ValuableForObjectBool;
	User->PublicUserFloat = ValuableForObjectFloat;
}


