// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perk.h"
#include "UObject/NoExportTypes.h"
#include "PerkTree.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTUREPATTERNS_API UPerkTree : public UObject
{
	GENERATED_BODY()
public:

	void BuildTree(const FString& Scheme);

	void ParseScheme(const FString& Scheme);

	UPerk* ParsePerk(const FString& PerkScheme);

	void AddPerk(UPerk* NewPerk);

private:

	TArray<UPerk*> PerkArray;
};
