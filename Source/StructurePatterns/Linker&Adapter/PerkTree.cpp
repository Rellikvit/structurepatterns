// Fill out your copyright notice in the Description page of Project Settings.


#include "PerkTree.h"

void UPerkTree::BuildTree(const FString& Scheme)
{
	ParseScheme(Scheme);
}

UPerk* UPerkTree::ParsePerk(const FString& PerkScheme)
{
	//Adapting FString information to perk class;
	UPerk* NewPerk = NewObject<UPerk>(this);
	//Changing NewPerk variables after adapting string {NewPerk->Variable = NewVariable;
	return NewPerk;
}

void UPerkTree::AddPerk(UPerk* NewPerk)
{
	PerkArray.Add(NewPerk);
}

void UPerkTree::ParseScheme(const FString& Scheme)
{
	//Parsing Scheme to PerkTree;
}
