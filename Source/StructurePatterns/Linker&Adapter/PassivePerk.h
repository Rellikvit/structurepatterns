// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Perk.h"
#include "PassivePerk.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTUREPATTERNS_API UPassivePerk : public UPerk
{
	GENERATED_BODY()

public:

	virtual void OpenAbility() override;
	
};
