// Fill out your copyright notice in the Description page of Project Settings.


#include "Perk.h"

bool UPerk::GetIsLearned() const
{
	return IsLearned;
}

void UPerk::Learn()
{
	IsLearned = true;
	OpenAbility();
}

void UPerk::OpenAbility()
{
	//Open ability with assigned perk;
}
