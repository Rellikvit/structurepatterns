// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "UObject/NoExportTypes.h"
#include "Perk.generated.h"

/**
 * 
 */
UCLASS()
class STRUCTUREPATTERNS_API UPerk : public UObject
{
	GENERATED_BODY()

public:

	virtual bool GetIsLearned() const;

	virtual void Learn();

	virtual void OpenAbility();
	
protected:
	
	bool IsLearned;
	
};
