// Fill out your copyright notice in the Description page of Project Settings.


#include "NPCComponent.h"

// Sets default values for this component's properties
UNPCComponent::UNPCComponent()
{
	//Initialize NPC
}

void UNPCComponent::BeginInteraction()
{
	BeginTalk();
	if (isQuest) GiveQuest();
	if (isShop) OpenShop();
}

void UNPCComponent::BeginTalk()
{
	//Talk with player
}

void UNPCComponent::GiveQuest()
{
	//Give player a quest
}

void UNPCComponent::OpenShop()
{
	//Open shop for player
}

