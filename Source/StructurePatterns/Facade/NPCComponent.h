// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "NPCComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class STRUCTUREPATTERNS_API UNPCComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UNPCComponent();

	UFUNCTION()
	virtual void BeginInteraction();

	UFUNCTION()
	virtual void BeginTalk();
	
	UFUNCTION()
	virtual void GiveQuest();

	UFUNCTION()
	virtual void OpenShop();

protected:
	bool isQuest;
	bool isShop;
};
